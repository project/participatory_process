<?php

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\participatory_process\Utility\ParticipationHelper;

/**
 * Implements hook_entity_field_access().
 */
function participatory_process_entity_field_access($operation, FieldDefinitionInterface $field_definition, AccountInterface $account, FieldItemListInterface $items = NULL) {
  // By default neutral acces result.
  $result = AccessResult::neutral();

  if ($operation === 'edit') {
    $field_name = $field_definition->getName();
    switch ($field_name) {
      case 'field_it_approval_system':
      case 'field_it_num_electable_ideas':
      case 'field_it_min_idea_support':
      case 'field_it_min_idea_votes':
      case 'field_it_target_teams':
      case 'field_it_proposals_date_range':
      case 'field_it_voting_date_range':
        if (!$account->hasPermission('it manage process settings')) {
          $result = AccessResult::forbidden();
        }
        break;

      case 'field_it_current_stage':
        if (!$account->hasPermission('it manage process stages')) {
          $result = AccessResult::forbidden();
        }
        break;

      case 'field_it_idea_status':
        if (!$account->hasPermission('it approve ideas')) {
          $result = AccessResult::forbidden();
        }
        break;
    }
  }

  return $result->addCacheContexts(['user.permissions']);
}

/**
 * Implements hook_theme().
 */
function participatory_process_theme($existing, $type, $theme, $path) {
  return [
    'block__info_card' => [
      'variables' => [
        'card_header'   => NULL,
        'card_body'     => NULL,
        'content_list'  => [],
        'content_items' => [],
        'card_footer'   => NULL,
      ],
      'template' => 'block--info-card',
    ],
  ];
}

/**
 * Implements hook_cron().
 */
function participatory_process_cron() {
  $last_run = \Drupal::state()->get('participatory_process.last_cron_run', 0);

  // Run cron only one time every an hour.
  if ((REQUEST_TIME - $last_run) > 3600) {
    $changing_stages = ParticipationHelper::getChangingStagesKeys();
    $entity_storage = \Drupal::entityManager()->getStorage('node');

    $ids = $entity_storage->getQuery()
      ->condition('type', 'it_participatory_process')
      ->condition('field_it_current_stage', $changing_stages, 'IN')
      ->accessCheck(FALSE)
      ->execute();

    drupal_set_message(serialize($ids));
    if (!empty($ids)) {
      if(!empty($entities = $entity_storage->loadMultiple($ids))) {
        $updated_ids = [];
        foreach ($entities as $entity) {
          if (ParticipationHelper::updateStage($entity)) {
            $updated_ids[] = (int) $entity->id();
          }
        }

        if (!empty($updated_ids)) {
          \Drupal::logger('participatory_process')->info(new FormattableMarkup('Participation process stages updated in nids: @ids.', [
            '@ids' => implode(', ', $updated_ids),
          ]));
        }
      }
    }

    // Update last cron run.
    \Drupal::state()->set('participatory_process.last_cron_run', REQUEST_TIME);
  }
}

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 */
function participatory_process_form_node_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  switch ($form_id) {
    case 'node_it_participatory_process_form':
    case 'node_it_participatory_process_edit_form':
      $form['#validate'][] = '_participatory_process_validate_participation_settings';
      break;
  }
}

/**
 * Validates participation settings.
 */
function _participatory_process_validate_participation_settings($form, FormStateInterface &$form_state) {
  $proposals_dates = $form_state->getValue('field_it_proposals_date_range');
  $voting_dates = $form_state->getValue('field_it_voting_date_range');

  if ($proposals_dates && $voting_dates) {
    $proposals_start = strtotime($proposals_dates[0]['value'] . ' UTC');
    $proposals_end = strtotime($proposals_dates[0]['end_value'] . ' UTC');
    $voting_start = strtotime($voting_dates[0]['value'] . ' UTC');
    $voting_end = strtotime($voting_dates[0]['end_value'] . ' UTC');

    if ($proposals_start > $voting_start) {
      $form_state->setErrorByName('field_it_voting_date_range', t('Voting cannot start before submitting proposals.'));
    }
    if ($proposals_end > $voting_end) {
      $form_state->setErrorByName('field_it_proposals_date_range', t('You cannot accept new proposals after voting ends.'));
    }
  }
}
