<?php

namespace Drupal\participatory_process\Utility;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\node\NodeInterface;


/**
 * Helper class for participation management.
 */
class ParticipationHelper {

  /**
   * Get info field build.
   */
  public static function build($options) {
    $build = [];

    if (!empty($options['label'])) {
      if (!empty($options['picto'])) {
        $build['label'] = new FormattableMarkup('<i class="@picto"></i> @label', [
          '@picto' => $options['picto'],
          '@label' => $options['label'],
        ]);
      }
      else {
        $build['label'] = $options['label'];
      }
    }

    $build['value'] = !empty($options['value']) ? $options['value'] : '';

    return $build;
  }

  /**
   * Get public stages list in numeric order.
   */
  public static function getPublicStages() {
    // Skip draft stage.
    return [
      1 => [
        'values' => ['exhibition'],
        'label'  => t('Information'),
      ],
      2 => [
        'values' => ['proposals'],
        'label'  => t('Collecting proposals'),
      ],
      3 => [
        'values' => ['voting', 'proposals_voting'],
        'label'  => t('Open voting'),
      ],
      4 => [
        'values' => ['results'],
        'label'  => t('Process finished'),
      ]
    ];
  }

  /**
   * Get changing stages keys.
   */
  public static function getChangingStagesKeys() {
    return [
      'exhibition',
      'proposals',
      'voting',
      'proposals_voting',
    ];
  }

  /**
   * Updates participation stage entity using entity field settings.
   */
  public static function updateStage(NodeInterface $node) {
    if ('it_participatory_process' === $node->getType()) {
      $current_stage = $node->get('field_it_current_stage')->getValue();
      $proposals_dates = $node->get('field_it_proposals_date_range')->getValue();
      $voting_dates = $node->get('field_it_voting_date_range')->getValue();

      if ($current_stage && $proposals_dates && $voting_dates) {
        $current_stage = isset($current_stage[0]['value']) ? $current_stage[0]['value'] : '_none';
        $changing_stages = self::getChangingStagesKeys();

        if (in_array($current_stage, $changing_stages)) {
          $proposals_start = strtotime($proposals_dates[0]['value'] . ' UTC');
          $proposals_end = strtotime($proposals_dates[0]['end_value'] . ' UTC');
          $voting_start = strtotime($voting_dates[0]['value'] . ' UTC');
          $voting_end = strtotime($voting_dates[0]['end_value'] . ' UTC');

          $new_stage = 'exhibition';
          if (REQUEST_TIME >= $proposals_start && REQUEST_TIME < $proposals_end) {
            if (REQUEST_TIME >= $voting_start && REQUEST_TIME < $voting_end) {
              $new_stage = 'proposals_voting';
            }
            else {
              $new_stage = 'proposals';
            }
          }
          elseif (REQUEST_TIME >= $voting_start && REQUEST_TIME < $voting_end) {
            $new_stage = 'voting';
          }
          elseif (REQUEST_TIME > $proposals_end && REQUEST_TIME > $voting_end) {
            $new_stage = 'results';
          }

          if ($current_stage != $new_stage) {
            $node->set('field_it_current_stage', $new_stage);
            $node->save();

            return TRUE;
          }
        }
      }
    }

    return FLASE;
  }

}
