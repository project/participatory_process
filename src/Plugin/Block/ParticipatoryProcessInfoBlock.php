<?php

namespace Drupal\participatory_process\Plugin\Block;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\participatory_process\Utility\ParticipationHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @Block(
 *   id = "participatory_process_info_block",
 *   admin_label = @Translation("About the Participatory process"),
 * )
 */
class ParticipatoryProcessInfoBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The theme manager.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected $themeManager;



  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'label_display' => 0,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    if ($node = $this->routeMatch->getParameter('node')) {
      // If we are in a idea try to load the target participatory process.
      if ('it_idea' === $node->getType()) {
        $participatory_process = $node->get('field_it_participatory_process')->getValue();
        if (isset($participatory_process[0]['target_id'])) {
          $node = $this->entityManager->getStorage('node')->load($participatory_process[0]['target_id']);
        }
      }

      if ('it_participatory_process' === $node->getType()) {
        $current_stage = $node->get('field_it_current_stage')->getValue();
        $proposals_dates = $node->get('field_it_proposals_date_range')->getValue();
        $voting_dates = $node->get('field_it_voting_date_range')->getValue();

        // Build the block if we have all data avaible.
        if ($current_stage && $proposals_dates && $voting_dates) {
          $current_stage = isset($current_stage[0]['value']) ? $current_stage[0]['value'] : '_none';

          // Current stage markup.
          $body  = '<p>' . $node->toLink()->toString() . '</p>';
          $body .= '<div class="it-fase text-secondary text-center">';
          if ('draft' === $current_stage) {
            $body .= '<p>' . $this->t('This participation process is a draft. Put in "Public exhibition" stage to starts the participation process.') . '</p>';
          }
          else {
            $stages = ParticipationHelper::getPublicStages();

            $current_stage_finish = '';
            $current_stage_label = '';
            $next_stage_start = '';
            $next_stage_label = '';

            switch ($current_stage) {
              case 'exhibition':
                $next_stage_start = $proposals_dates[0]['value'];
                $current_stage_label = $stages[1]['label'];
                $next_stage_label = $stages[2]['label'];
                break;

              case 'proposals':
                $current_stage_finish = $proposals_dates[0]['end_value'];
                $next_stage_start = $voting_dates[0]['value'];
                $current_stage_label = $stages[2]['label'];
                $next_stage_label = $stages[3]['label'];
                break;

              case 'voting':
              case 'proposals_voting':
                $current_stage_finish = $voting_dates[0]['end_value'];
                $current_stage_label = $stages[3]['label'];
                $next_stage_label = $stages[4]['label'];
                break;

              case 'results':
                $current_stage_label = $stages[4]['label'];
                break;
            }

            $body .= '<p>' . $this->t('Current stage') . '</p>';
            foreach ($stages as $key => $stage) {
              if (in_array($current_stage, $stage['values'])) {
                $body .= '<span class="fa-stack fa-lg" title="' . $stage['label'] . '">';
                $body .= '<i class="fas fa-circle fa-stack-2x"></i>';
                $body .= '<span class="fa-stack-1x fa-inverse">' . $key . '</span>';
                $body .= '</span>';
              }
              else {
                $body .= '<span class="fa-stack" title="' . $stage['label'] . '">';
                $body .= '<i class="far fa-circle fa-stack-2x"></i>';
                $body .= '<span class="fa-stack-1x">' . $key . '</span>';
                $body .= '</span>';
              };
            }

            $body .= '<p>' . $current_stage_label;
            if (!empty($current_stage_finish)) {
              $body .= '<br>';
              $body .= $this->t('Until: @date', [
                '@date' => $this->dateFormatter->format(strtotime($current_stage_finish . ' UTC'), 'short'),
              ]);
            }
            $body .= '</p>';
          }
          $body .= '</div><hr>';

          // Next stage markup.
          if (!empty($next_stage_label) || !empty($next_stage_start)) {
            $body .= '<p>';
            if (!empty($next_stage_label)) {
              $body .= $this->t('Next stage: @label', [
                '@label' => $next_stage_label,
              ]);
            }
            if (!empty($next_stage_start)) {
              $body .= '<br>';
              $body .= $this->t('Starts: @date', [
                '@date' => $this->dateFormatter->format(strtotime($next_stage_start . ' UTC'), 'short'),
              ]);
            }
            $body .= '</p><hr>';
          }

          // Info fields.
          $info_fields = [];

          if ($field = $node->get('field_it_num_electable_ideas')) {
            $info_fields[] = ParticipationHelper::build([
              'label' => $this->t('Electable ideas'),
              'picto' => 'fas fa-cog',
              'value' => $field->getValue()[0]['value'],
            ]);
          }
          if ($field = $node->get('field_it_min_idea_support')) {
            $info_fields[] = ParticipationHelper::build([
              'label' => $this->t('Minimum support'),
              'picto' => 'fas fa-cog',
              'value' => $field->getValue()[0]['value'],
            ]);
          }
          if ($field = $node->get('field_it_min_idea_votes')) {
            $info_fields[] = ParticipationHelper::build([
              'label' => $this->t('Minimum votes'),
              'picto' => 'fas fa-cog',
              'value' => $field->getValue()[0]['value'],
            ]);
          }

          $info_fields[] = ParticipationHelper::build([
            'label' => $this->t('Finish'),
            'picto' => 'far fa-calendar',
            'value' => $this->dateFormatter->format(strtotime($voting_dates[0]['end_value'] . ' UTC'), 'short'),
          ]);

          $info_fields[] = ParticipationHelper::build([
            'label' => $this->t('Author'),
            'picto' => 'fas fa-user',
            'value' => $node->getOwner()->getDisplayName(),
          ]);

          $build = [
            '#theme' => 'block__info_card',
            '#card_header' => new FormattableMarkup('<i class="fas fa-users"></i> @label', [
              '@label' => $this->t('About the Participatory process'),
             ]),
            '#card_body' => ['#markup' => $body],
            '#content_items' => $info_fields,
          ];

          // Add fontawesome library if it's not using ideastorm theme.
          if ('ideastorm' !== $this->themeManager->getActiveTheme()->getName()) {
            $build['#attached']['library'] = ['participatory_process/fontawesome_cdn'];
          }

          return $build;
        };
      }
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return Cache::mergeTags(
      parent::getCacheTags(), ['node_list']
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(
      parent::getCacheContexts(), ['url.path']
    );
  }

  /**
   * Class constructor.
   */
  public function __construct($configuration, $plugin_id, $plugin_definition, DateFormatterInterface $date_formatter, EntityTypeManagerInterface $entity_manager, RouteMatchInterface $route_match, ThemeManagerInterface  $theme_manager) {
    $this->dateFormatter = $date_formatter;
    $this->entityManager = $entity_manager;
    $this->routeMatch    = $route_match;
    $this->themeManager  = $theme_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('date.formatter'),
      $container->get('entity_type.manager'),
      $container->get('current_route_match'),
      $container->get('theme.manager')
    );
  }

}
