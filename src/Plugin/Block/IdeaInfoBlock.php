<?php

namespace Drupal\participatory_process\Plugin\Block;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\participatory_process\Utility\ParticipationHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @Block(
 *   id = "idea_info_block",
 *   admin_label = @Translation("About the Idea"),
 * )
 */
class IdeaInfoBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The theme manager.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected $themeManager;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'label_display' => 0,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $node = $this->routeMatch->getParameter('node');
    if ($node && 'it_idea' === $node->getType()) {
      $info_fields = [];

      $participatory_process = $node->get('field_it_participatory_process')->getValue();
      if (isset($participatory_process[0]['target_id'])) {
        $target_entity = $this->entityManager->getStorage('node')->load($participatory_process[0]['target_id']);

        $info_fields[] = ParticipationHelper::build([
          'label' => $node->get('field_it_participatory_process')->getFieldDefinition()->getLabel(),
          'value' => $target_entity->toLink()->toString(),
        ]);
      }

      $info_fields[] = ParticipationHelper::build([
        'label' => $this->t('Author'),
        'picto' => 'fas fa-user',
        'value' => $node->getOwner()->getDisplayName(),
      ]);

      if ($field = $node->get('field_it_idea_status')) {
        $info_fields[] = ParticipationHelper::build([
          'label' => $this->t('Status'),
          'value' => $field->getValue()[0]['value'],
        ]);
      }

      $build = [
        '#theme' => 'block__info_card',
        '#card_header' => new FormattableMarkup('<i class="far fa-lightbulb"></i> @label', [
          '@label' => $this->t('About the Idea'),
        ]),
        '#content_items' => $info_fields,
      ];

      // Add fontawesome library if it's not using ideastorm theme.
      if ('ideastorm' !== $this->themeManager->getActiveTheme()->getName()) {
        $build['#attached']['library'] = ['participatory_process/fontawesome_cdn'];
      }

      return $build;
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return Cache::mergeTags(
      parent::getCacheTags(), ['node_list']
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(
      parent::getCacheContexts(), ['url.path']
    );
  }

  /**
   * Class constructor.
   */
  public function __construct($configuration, $plugin_id, $plugin_definition, DateFormatterInterface $date_formatter, EntityTypeManagerInterface $entity_manager, RouteMatchInterface $route_match, ThemeManagerInterface  $theme_manager) {
    $this->dateFormatter = $date_formatter;
    $this->entityManager = $entity_manager;
    $this->routeMatch    = $route_match;
    $this->themeManager = $theme_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('date.formatter'),
      $container->get('entity_type.manager'),
      $container->get('current_route_match'),
      $container->get('theme.manager')
    );
  }

}
